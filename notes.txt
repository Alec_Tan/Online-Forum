= Assignment Notes =

== Application Layer Protocol ==

=== Process ===
MAP - Minimal Application Protocoll

- Server Side
	- Always On
	- Server program receives message from client via TCP
	- Server reads code given by the message
	- Server program maps message to function that handles given message format

- Client
	- Connection is create upon start of Client
	- Client program reads code given by command 
	- Client program maps code to function that generates the appropriate message given the correct command
	- Function then generates the appropriate message then sends it to the server via TCP


== CODES ==

=== AUTH ===

# LOGIN #
command: USR
Headers:
	1. Username

# AUTHORISE #
command: PWD
Headers:
	1. Password

# Create Log in Details #
code: NEW
Headers:
	1. Username
	2. Password


=== FORUM ACTIONS ===

# CREATE THREAD #
code: CRT
Headers:
	1. code
	2. username
	3. name (of thread)

# CREATE MSG #
code: MSG
Headers:
	1. code
	2. username
	


== RESPONSES ==

# OK response # 
Code: 200
Headers:
	1. Message

# Confirm New Details #
Code: 201
Headers:
	1. Username
	2. Password

# Confirm Login # 
Code: 202
Headers:
	1. code
	2. msg

# BAD REQUEST #
Code: 400
Headers: 
	1. Message

# INCORRECT USERNAME #
Code: 401
Headers:
	1. Username

# INCORRECT PASSWORD #
Code: 402
Headers:
	1. Username
	2. Password



