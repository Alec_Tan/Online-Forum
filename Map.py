# By Alec Tan (z5117181)

from socket import *
import os
import json

# DEFAULTS
DEF_HOSTNAME = 'localhost'
DEF_ADMIN_PASS = "password"
DEF_SERVER_IP = '127.0.0.1'
DEF_PORT = 12005
DEF_MAX_CONS = 1
DEF_MSG_LEN = 1024
DEF_MAX_REQS = 10
EF_MSG_LEN = 1024
DEF_CRED_PATH = "./credentials.txt"
SVR_DIR = '.'

class MAPServer():

    def __init__(self, serverHostname=DEF_HOSTNAME, serverPort=DEF_PORT, admin_pass=DEF_ADMIN_PASS):
        
        self.hostname = serverHostname
        self.port = int(serverPort)
        self.admin_pass = admin_pass
        self.creds = self.load_creds()
        
        # Initialise Socket
        self.socket = socket(AF_INET, SOCK_STREAM)
        # Bind IP and port to socket
        self.socket.bind((self.hostname, self.port))
        # Initialise Thread Directory
        self.active_threads = list()


    def listen(self, max_cons=DEF_MAX_CONS, msg_len=DEF_MSG_LEN, max_requests=DEF_MAX_REQS, limit_reqs=True):
        """
        Tell the server to start listening and handle requests.
        It will only handly a max of 10 requests by default 
        """
        print("waiting for clients")
        self.socket.listen(max_cons)

        reqs_handled = 0
        while True:
            conn_socket, addr = self.socket.accept()
            # Decodes Msg and creates a dictionary/json object
            msg_rcv = json.loads(conn_socket.recv(msg_len).decode('utf-8'))
            response = self.create_response(msg_rcv)
            conn_socket.send(response)

            conn_socket.close()

            reqs_handled += 1

            # For Testing Purposes
            if limit_reqs and reqs_handled >= max_requests:
                break

   
    def load_creds(self, path=DEF_CRED_PATH):

        creds = dict()
        cred_file = open(path, 'r')
        for line in cred_file:
            username, password = line.strip().split()
            creds[username] = password

        cred_file.close()

        return creds


    def create_response(self, msg_rcv):
        """
        Creates the appropriate MAP response to the given MAP msg
        """
        
        # Initialise Default Response Object - "400 Bad Request"
        response = self.create_bad_reqs_response()
        # Get Code from received Message
        cmd  = msg_rcv['command']

        # Map message to appropriate response
        if cmd == "USR":
            response = self.handle_login(msg_rcv)

        elif cmd == 'PWD':
            response = self.handle_auth(msg_rcv)

        elif cmd == 'NEW':
            response = self.handle_new_details(msg_rcv)

        elif cmd == 'CRT':
            response = self.handle_new_thread(msg_rcv)

        elif cmd == 'MSG':
            response = self.handle_message(msg_rcv)

        elif cmd == 'LST':
            response = self.handle_list(msg_rcv)
        
        elif cmd == 'DLT':
            response = self.handle_delete(msg_rcv)

        elif cmd == 'RDT':
            response = self.handle_read(msg_rcv)

        elif cmd == 'EDT':
            response = self.handle_edit(msg_rcv)

        elif cmd == 'CHK':
            response = self.handle_check(msg_rcv)

        elif cmd == 'UPD':
            response = self.handle_upload(msg_rcv)

        elif cmd == 'DWN':
            response = self.handle_download(msg_rcv)

        elif cmd == 'RMV':
            response = self.handle_remove(msg_rcv)


        return response

  
    def handle_login(self, msg_rcv):
        """
        Decides what response to create when user attempts to login
        Code: 100
        """

        username_given = msg_rcv['username']
        
        if username_given in self.creds:
            return self.create_ok_response("Username Found")
    
        return self.create_incorrect_details_response()


    def handle_new_details(self, msg_rcv):
        """
        Decides what response to create when uses creates a new account in sever.
        Also handles account creation.
        Code: 102
        """

        new_username = msg_rcv['username']
        new_password = msg_rcv['password']

        self.creds[new_username] = new_password

        cred_file = open(DEF_CRED_PATH, 'a')
        new_details = new_username + " " + new_password +"\n"
        cred_file.write(new_details)

        return self.create_ok_response("Account Successfully Created")


    def handle_auth(self, msg_rcv):
        """
        Decides what response to create when user provides password for authentication.
        Code: 101
        """

        username_given = msg_rcv['username']
        password_given = msg_rcv['password'] 
        if self.creds[username_given] == password_given:
            print(username_given, "successfully logged in")
            return self.create_ok_response("{user} Successfully logged in".format(user=username_given))

        return self.create_incorrect_details_response()

 
    def handle_message(self, msg_rcv):
        """
        Decides what response to create when user attempts to create a 
        new message in a thread.
        Code: 
        """

        author = msg_rcv['username']
        thread = msg_rcv['thread']
        msg = msg_rcv['message']

        if self.thread_exists(thread):
            thread_obj = self.get_thread(thread)
            thread_obj.post_msg(author, msg)
            return self.create_ok_response("Message Successfully Posted")

        return self.create_no_resource_response(thread)


    def handle_new_thread(self, msg_rcv):
        
        username = msg_rcv['username']
        name = msg_rcv['name']

        print(username, "issues a CRT command")
        
        if self.thread_exists(name):
            print("Thread " + name + " exists")
            return self.create_thread_exist_response(name)
       
        
        thread = open(name, 'w')
        thread.write(username + "\n\n")
        self.active_threads.append(Thread(name, username))
        print("Thread " + name + " created")

        thread.close()

        return self.create_ok_response("Thread successfully created")

   
    def thread_exists(self, thread_name):
        """
        Checks if a thread exists with the given name
        Returns: Bool
        """

        for thread in self.active_threads:
            if thread.name == thread_name:
                return True

        return False 


    def get_thread(self, thread_name):
        """
        Returns Thread with the given name
        """

        for thread in self.active_threads:
            if thread.name == thread_name:
                return thread

        return None

    
    def handle_list(self, msg_rcv):
        thread_names = list()
        for thread in self.active_threads:
            thread_names.append(thread.name)

        return self.create_ok_response(thread_names)


    def handle_delete(self, msg_rcv):

        thread_name = msg_rcv['thread']
        msg_num = msg_rcv['msg_num']
        username = msg_rcv['username']
    
        print("{user} requests to delete message # {num} from {thread}".format(user=username, num=msg_num, thread=thread_name))
        thread_obj = self.get_thread(thread_name)

        if thread_obj == None:
            return self.create_no_resource_response(thread_name)

        if not thread_obj.delete_msg(msg_num, username):
            print("Delete Unsucessful")
            return self.create_no_resource_response("Message No. {num} from {user} in {thread_name}".format(num=msg_num,
                                                                                                            user=username))

        print("Delete Successful")

        return self.create_ok_response("Message Successfully Deleted") 

    def handle_read(self, msg_rcv):

        thread_name = msg_rcv['thread']
        user = msg_rcv['user']

        print("{user} requests to read {thread}".format(user=user, thread=thread_name))

        thread = self.get_thread(thread_name)
        
        if thread == None:
            print("{thread} does not exist".format(thread=thread))
            return self.create_no_resource_response(thread_name)

        contents = thread.read_thread()

        print("Read Successful")
        return  self.create_ok_response(contents)

    
    def handle_edit(self, msg_rcv):
        thread_name = msg_rcv['thread']
        msg_num = msg_rcv['msg_num']
        new_msg = msg_rcv['new_msg']
        user = msg_rcv['user']

        thread = self.get_thread(thread_name)

        if thread == None:
            return self.create_no_resource_response(thread_name)

        if thread.edit_msg(msg_num, user, new_msg):
            return self.create_ok_response("Message Succesffully Edited")

        return self.create_create_no_resource_response("{Message No. {num} from {user} in {thread}".format(msg_num, user, thread_name))
   
    def handle_check(self, msg_rcv): 

        thread_name = msg_rcv['thread']

        if self.thread_exists(thread_name):
            return self.create_thread_exist_response(thread_name)

        return  self.create_no_resource_response(thread_name)

    def handle_upload(self, msg_rcv):

        thread_name = msg_rcv['thread']
        filename = msg_rcv['filename']
        content_list = msg_rcv['content']
        username = msg_rcv['user']
    
        print("{user} uploaded {filename}".format(user=username, filename=filename))

        # create server's copy of file
        thread_obj = self.get_thread(thread_name)
        thread_obj.create_file(filename, content_list, username)
        
        response_msg = "{filename} uploaded to {thread}".format(filename=filename, thread=thread_name)

        return self.create_ok_response(response_msg)

    
    def handle_download(self, msg_rcv):
        
        thread_name = msg_rcv['thread']
        filename = msg_rcv['filename']

        thread_obj = self.get_thread(thread_name)
        if thread_obj == None:
            return self.create_no_resource_response(thread_name)

        elif not thread_obj.has_file(filename):
            return self.create_no_resource_response(filename)

        contents_list = thread_obj.get_file_contents(filename)
        
        return self.create_ok_response(contents_list)

    def handle_remove(self, msg_rcv):

        thread_name = msg_rcv['thread']
        username = msg_rcv['user']
    
        thread_obj = self.get_thread(thread_name)

        if thread_obj == None:
            return self.create_no_resource_response(thread_name)

        elif thread_obj.user != username:
            return self.create_request_denied_response()

        thread_obj.delete_uploaded_files()
        os.remove(thread_name)


        for thread_obj in self.active_threads:
            if thread_obj.name == thread_name:
                self.active_threads.remove(thread_obj)
                break


        return self.create_ok_response("{thread} removed successfully".format(thread=thread_name))


    # ======= RESPONSES ========== #

    def create_ok_response(self, msg):

        response_obj = {'code' : '200', 
                        'msg'  : msg
                       }

        return json.dumps(response_obj).encode('utf-8')


    def create_bad_reqs_response(self):

        
        response_obj = {'code' : '400',
                        'msg' : 'Bad Request'
                       }

        return json.dumps(response_obj).encode('utf-8')


    def create_incorrect_details_response(self):
       
        response_obj = {'code' : '401',
                        'msg'  : "Incorrect Login Details"
                       }

        return json.dumps(response_obj).encode('utf-8')
    
    def create_thread_exist_response(self, name):

        response_obj = { 'code' : '402', 
                         'msg'  : 'Thread "' + name + '" exists'
                       }

        return json.dumps(response_obj).encode('utf-8') 


    def create_no_resource_response(self, resource_name):

        response_obj = {'code' : '404',
                        'msg'  : "{resource} does not exist".format(resource=resource_name)
                       }

        return json.dumps(response_obj).encode('utf-8')

    def create_request_denied_response(self):

        response_obj = {'code' : '403',
                        'msg'  :  "Request Denied"
                       }

        return json.dumps(response_obj).encode('utf-8')

    
class MAPClient():

    def __init__(self, server_ip=DEF_SERVER_IP, server_port=DEF_PORT):
        self.server_ip=server_ip
        self.server_port=server_port

    def login(self):

        username = ""
        while username == "":
            username = input("username: ")
        
        login_msg = self.create_login_request(username)
        response = self.send_msg(login_msg)

        if response['code'] != '200':
            print(response['msg'])
            return False

        password = ""
        while password == "":
            password = input("password: ")


        auth_msg = self.create_auth_request(username, password)
        response = self.send_msg(auth_msg)

        if response['code'] != '200':
            print(response['msg'])
            return False

        return True     
   

    def create_login_request(self, username):
        
        login_obj = {'command'     : 'USR',
                     'username' : username
                    } 

        login_msg = json.dumps(login_obj).encode('utf-8')

        return login_msg
  

    def create_auth_request(self, username, password):
        
        auth_obj = {'command'  : 'PWD',
                    'username' : username,
                    'password' : password
                   }

        auth_msg = json.dumps(auth_obj).encode('utf-8')

        return auth_msg


    def create_new_details_request(self, username, password):

        req_obj = {'command' : 'NEW',
                   'username' : username, 
                   'password' : password
                  }

        return json.dumps(req_obj).encode('utf-8')


    def create_thread_request(self, username, thread_name):

        req_obj = {'command' : 'CRT',
                   'username' : username, 
                   'name'     : thread_name
                  }

        return json.dumps(req_obj).encode('utf-8')


    def post_msg(self, thread, user, message):

        req_obj = {'command' : 'MSG',
                   'username' : user,
                   'thread'   : thread,
                   'message'  : message
                  }

        return json.dumps(req_obj).encode('utf-8')

    def list_request(self):

        req_obj = {'command' : 'LST'}

        return json.dumps(req_obj).encode('utf-8')

    def create_delete_request(self, thread, msg_num, username):

        req_obj = {'command' : 'DLT', 
                   'username' : username,
                   'thread' : thread,
                   'msg_num' : msg_num,
                  }

        return json.dumps(req_obj).encode('utf-8')

    def create_read_request(self, thread_name, username):


        req_obj = {'command' : 'RDT',
                   'thread'  : thread_name,
                   'user' : username
                   }

        return json.dumps(req_obj).encode('utf-8')

    def create_edit_request(self, thread_name, msg_num, new_msg, username):

        req_obj = {'command' : 'EDT',
                   'thread'  : thread_name,
                   'msg_num' : msg_num, 
                   'new_msg' : new_msg,
                   'user'    : username
                  }

        return json.dumps(req_obj).encode('utf-8')

    def create_check_thread_request(self, thread_name):
        
        req_obj = {'command' : 'CHK',
                   'thread'  : thread_name
                  }

        return json.dumps(req_obj).encode('utf-8')


    def create_upload_request(self, thread_name, filename, username):

        file_obj = open(filename, 'r')
        content = file_obj.readlines() 
        req_obj = {'command'  : 'UPD',
                   'thread'   : thread_name, 
                   'filename' : filename,
                   'content'  : content,
                   'user'     : username
                  } 

        return json.dumps(req_obj).encode('utf-8')


    def create_download_request(self, thread_name, filename, username):

        req_obj = {'command'  : 'DWN',
                   'thread'   : thread_name, 
                   'filename' : filename, 
                   'user'     : username
                  } 

        return json.dumps(req_obj).encode('utf-8')

    
    def create_remove_request(self, thread_name, username):

        req_obj = {'command' : 'RMV',
                   'thread'  : thread_name, 
                   'user'    : username
                  }

        return json.dumps(req_obj).encode('utf-8')


    def send_msg(self, msg, msg_len=DEF_MSG_LEN):

        clientSocket = socket(AF_INET, SOCK_STREAM)
        clientSocket.connect((self.server_ip, self.server_port))
            
        # Send Message
        clientSocket.send(msg)

         # Wait for response
        raw_response = clientSocket.recv(msg_len)
        response_obj = json.loads(raw_response.decode('utf-8'))
                    
        clientSocket.close()
                
        return response_obj


class Thread():

    def __init__(self, thread_name, user):
        self.name = thread_name
        self.user = user
        self.num_msgs = 0
        self.files = list()

    def read_thread(self):

        file_obj = open(self.name,'r')
        lines = file_obj.readlines()[2:]
        file_obj.close() 

        return lines

    def post_msg(self, username, message):

        thread = open(self.name, 'a')
        new_msg = "{msg_num} {user}: {msg}\n".format(msg_num=self.num_msgs + 1,
                                                   user=username,
                                                   msg=message)
        thread.write(new_msg)
        self.num_msgs += 1  
        thread.close()


    def edit_msg(self, msg_num, user, new_msg):
    
        file_obj = open(self.name, 'r')
        tmp_file = open(self.name + '.tmp', 'a')
        
        found = False
        for line in file_obj:

            words = line.split()

            if len(words) == 0:
                tmp_file.write('\n')
            
            elif len(words) == 1:
                tmp_file.write(line)

            elif words[0] == msg_num and words[1] == (user + ':'):
                found = True
                new_msg = "{num} {user}: {msg}\n".format(num=msg_num, user=user, msg=new_msg)
                tmp_file.write(new_msg)
                   
            else:
                tmp_file.write(line)

        file_obj.close()
        tmp_file.close()

        if found:
            os.remove(self.name)
            os.rename(self.name + '.tmp', self.name)
            return True

        return False

    def delete_msg(self, msg_num, user):
        file_obj = open(self.name, 'r')
        
        new_lines = list()
        found = False
        for line in file_obj:
       
            words = line.split()

            # If empty line, add empty line to new lines
            if len(words) == 0:
                new_lines.append('\n\n')
            # If found thread author, add to new_lines     
            elif len(words) == 1:
                new_lines.append(words[0])

            elif words[0] == msg_num and words[1] == (user + ":"):
                found = True

            elif found:
                new_msg_num = str(int(words[0]) - 1)
                words[0] = new_msg_num
                new_line = ' '.join(words)
                new_line = new_line + '\n'
                new_lines.append(new_line)
            
            else:
                new_lines.append(line)

        file_obj.close()
       
        # If message was found, new_lines is valid - create new thread file
        if found:
            file_obj = open(self.name, 'w')
            file_obj.writelines(new_lines)
            file_obj.close()
            return True

        return False

    def create_file(self, filename, content, username):

        server_filename = "{thread}-{filename}".format(thread=self.name, filename=filename) 
        file_obj = open(server_filename, 'w')
        file_obj.writelines(content)

        file_obj.close()

        file_obj = open(self.name, 'a')
        msg = '{user} uploaded {filename}\n'.format(user=username, filename=filename)
        file_obj.write(msg)

        file_obj.close() 

        self.files.append(filename)

    def has_file(self, filename):
        
        server_filename = "{thread}-{filename}".format(thread=self.name, filename=filename)
        return filename in self.files

    def get_file_contents(self, filename):

        server_filename = "{thread}-{filename}".format(thread=self.name, filename=filename)
        file_obj = open(server_filename, 'r')

        contents_list = file_obj.readlines()
        file_obj.close()

        return contents_list 

    def delete_uploaded_files(self):

        for filename in self.files:
            server_filename = "{thread}-{filename}".format(thread=self.name, filename=filename)
            os.remove(server_filename)

        
