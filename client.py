# TCP Client - Python3
# By: Alec Tan (z5117181)

import Map
import sys

CMDS = """
Enter one of the following commans:
CRT - Create Thread
MSG - Post Message
DLT - Delete Message
RDT - Read Thread
EDT - Edit Message
UPD - Upload File
DWN - Download File
RMV - Remove Thread
XIT - Exit
SHT - Shutdown Server
        
Type 'help <CMD>' for more info
Type 'help' for the list of commands
"""

client_obj = Map.MAPClient()
   
 
# Login Loop
while True:

    print("Loggin In")
    username = ""
    while not username:
        username = input("Username: ").strip()

    username_msg = client_obj.create_login_request(username)
    response = client_obj.send_msg(username_msg)
    
    if response['code'] == '401':
        print(username, " not found. Creating new account")
        new_pass = input("New password: ").strip()
        new_pass_msg  = client_obj.create_new_details_request(username, new_pass)
        response = client_obj.send_msg(new_pass_msg)
        print(response['msg'])
        continue
    
    password = ""
    while not password:
        password = input("password: ").strip()

    password_msg = client_obj.create_auth_request(username, password)
    response = client_obj.send_msg(password_msg)
    
    print(response['msg'])
    
    if response['code'] == '200':
        break

print(CMDS)
while True:

    cmd = input(">> ").strip()
    argv = cmd.split()
    argc = len(argv)
    

    if argv[0] == "help":
        print(CMDS)

    elif argc == 2 and argv[0] == "CRT":
        thread = argv[1]
        msg = client_obj.create_thread_request(username, thread)
        response = client_obj.send_msg(msg)
        print(response['msg'])  

    elif argc >= 3 and argv[0] == 'MSG':
        thread_name = argv[1]
        thread_msg = ' '.join(argv[2:])
        msg = client_obj.post_msg(thread_name, username, thread_msg)
        response = client_obj.send_msg(msg)
        print(response['msg'])

    elif argc == 1 and argv[0] == 'LST':
        msg = client_obj.list_request()
        response = client_obj.send_msg(msg)
        thread_names = response['msg']
        if len(thread_names) == 0:
            print("There are no active threads")
        else:
            print("Active Threads")
            for name in thread_names:
                print(name)

    elif argc == 3 and argv[0] == 'DLT':
        thread_name = argv[1]
        msg_num = argv[2]
        msg = client_obj.create_delete_request(thread_name, msg_num, username)
        response = client_obj.send_msg(msg)       
        print(response['msg']) 

    elif argc == 2 and argv[0] == 'RDT':
        thread_name = argv[1]
        msg = client_obj.create_read_request(thread_name, username)
        response = client_obj.send_msg(msg)

        if response['code'] != '200':
            print(response['msg']) 
            continue

        lines = response['msg']
        if len(lines) == 0:
            print("There no messsages in {thread}".format(thread=thread_name))
        else:
            for line in lines:
                print(line, end='')

    elif argc == 4 and argv[0] == 'EDT':
        thread_name = argv[1]
        msg_num = argv[2]
        new_msg = argv[3]
        msg = client_obj.create_edit_request(thread_name, msg_num, new_msg, username)
        response = client_obj.send_msg(msg)
        print(response['msg']) 

    elif argc == 3 and argv[0] == 'UPD':
        thread_name = argv[1]
        filename = argv[2]
        msg = client_obj.create_check_thread_request(thread_name)
        response = client_obj.send_msg(msg)
        print(response['msg'])
        if response['code'] != '402':
            continue

        msg = client_obj.create_upload_request(thread_name, filename, username)
        response = client_obj.send_msg(msg)
        print(response['msg'])

    elif argc == 3 and argv[0] == 'DWN':
        
        thread_name = argv[1]
        filename = argv[2]
        msg = client_obj.create_download_request(thread_name, filename, username)
        response = client_obj.send_msg(msg)

        if response['code'] != '200':
            print(response['msg'])
            continue

        file_contents = response['msg']
        file_obj = open(filename, 'w') 
        file_obj.writelines(file_contents)
        file_obj.close()

        print("{filename} successfully downloaded".format(filename=filename))
   
    elif argc == 2 and argv[0] == 'RMV':

        thread_name = argv[1]
        msg = client_obj.create_remove_request(thread_name, username)
        response = client_obj.send_msg(msg)
        print(response['msg'])

    
